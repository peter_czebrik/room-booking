package cz.czebrik.roombooking.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import cz.czebrik.roombooking.model.ConferenceRoom;
import cz.czebrik.roombooking.model.RoomFilter;
import cz.czebrik.roombooking.service.RoomService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RoomControllerTest {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@MockBean
	private RoomService roomService;

	@Before
	public void init() {
		when(roomService.findRoomByName(anyString(), any(HttpServletResponse.class))).thenReturn(getDummyRoom());
		when(roomService.searchBy(any(RoomFilter.class), any(HttpServletResponse.class))).thenReturn(getDummyRoom());
	}

	@Test
	public void testFindByName() {
		ResponseEntity<ConferenceRoom> response = restTemplate.getForEntity("http://localhost:" + port + "/rooms/r1", ConferenceRoom.class);
		assertEquals(200, response.getStatusCodeValue());
		assertEquals("r1", response.getBody().getRoomName());
	}

	@Test
	public void testSearchBy() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<>(getDummyFilterJson(), headers);
		ResponseEntity<ConferenceRoom> response = restTemplate.exchange("http://localhost:" + port + "/rooms/filter", HttpMethod.POST, entity, ConferenceRoom.class);
		assertEquals(200, response.getStatusCodeValue());
		assertEquals("r1", response.getBody().getRoomName());
	}

	private ConferenceRoom getDummyRoom() {
		return new ConferenceRoom("r1", "r1", "http://any.any/any", "123456", 1, true, true);
	}

	private String getDummyFilterJson() {
		return
				"{"
				+ "\"roomName\": \"r1\","
				+ "\"roomNumber\": \"\","
				+ "\"capacity\": \"\","
				+ "\"includesProjector\": false,"
				+ "\"includesFlipchart\": false"
				+ "}";
	}
}
