var roomObject;

function findRoomByName(name) {
	console.log("I am searching for room by name", name);
	fetch('http://localhost:8080/rooms/' + name).then(function (response) {
		console.log(response);
		if (response.status === 200) {
			return response.json();
		} else {
			return null;
		}
	}).then(function (jsonRes) {
		if (!jsonRes) {
			showNotFound();
			return;
		}
		roomObject = jsonRes;
		console.log("response object", roomObject);
		if (roomObject.roomName) {
			document.querySelector('#basic-search').setAttribute('hidden', 'true');
			document.querySelector('#show-result-summary').removeAttribute('hidden');
		}
	});
}

function searchBy() {
	event.preventDefault();
	var roomName = document.querySelector('#room-name').value;
	var roomNumber = document.querySelector('#room-number').value;
	var capacity = document.querySelector('#capacity').value;
	var includesProjector = document.querySelector('#projector-included').checked;
	var includesFlipchart = document.querySelector('#flipchart-included').checked;
	var formJson = JSON.stringify({
		"roomName": roomName,
		"roomNumber": roomNumber,
		"capacity": capacity,
		"includesProjector": includesProjector,
		"includesFlipchart": includesFlipchart,
		"stupidProperty": "stupidValue"
	});
	console.log("Sending POST request with fiter object", formJson);
	var xhr = new XMLHttpRequest();
	var url = "http://localhost:8080/rooms/filter";
	xhr.open("POST", url, true);
	xhr.setRequestHeader("Content-type", "application/json");
	xhr.onloadend = function () {
		if (xhr.status === 404) {
			showNotFound();
		}
	};
	xhr.onreadystatechange = function () {
		if (xhr.readyState === 4 && xhr.status === 200) {
			var json = JSON.parse(xhr.responseText);
			console.log("response to post", json);
			roomObject = json;
			console.log("response object", roomObject);
			if (roomObject.roomName) {
				document.querySelector('#show-result-summary').removeAttribute('hidden');
				document.querySelector('#room-details').setAttribute('hidden', 'true');
			}
		}
	};
	xhr.send(formJson);
}

function showNotFound() {
	console.log("No room found");
	window.alert("No room found :(");
}