function showDetails() {
	document.querySelector('#form-submit-button').setAttribute('hidden', 'true');
	document.querySelector('#show-result-summary').setAttribute('hidden', 'true');
	document.querySelector('#room-details').removeAttribute('hidden');
	document.querySelector('#phone-number-input').removeAttribute('hidden');
	document.querySelector('#room-map').removeAttribute('hidden');

	document.querySelector('#room-name').value = roomObject.roomName;
	document.querySelector('#room-number').value = roomObject.roomNumber;
	document.querySelector('#phone-contact').value = roomObject.phoneNumber;
	document.querySelector('#capacity').value = roomObject.capacity;
	document.querySelector('#projector-included').setAttribute('checked', roomObject.includesProjector);
	document.querySelector('#flipchart-included').setAttribute('checked', roomObject.includesFlipchart)
	document.querySelector('#room-map').setAttribute('src', roomObject.mapUrl);
}

function showAdvancedFilter() {
	document.querySelector('#room-details').removeAttribute('hidden');
	document.querySelector('#basic-search').setAttribute('hidden', 'true');
	document.querySelector('#phone-number-input').setAttribute('hidden', 'true');
	document.querySelector('#room-map').setAttribute('hidden', 'true');
}