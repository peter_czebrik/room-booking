package cz.czebrik.roombooking.controller;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cz.czebrik.roombooking.model.ConferenceRoom;
import cz.czebrik.roombooking.model.RoomFilter;
import cz.czebrik.roombooking.service.RoomService;

@RestController
@RequestMapping(path = "/rooms")
public class RoomController {

	private static Logger LOGGER = LoggerFactory.getLogger(RoomController.class);

	private final RoomService roomService;

	@Autowired
	public RoomController(RoomService roomService) {
		this.roomService = roomService;
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{name}")
	public ConferenceRoom getConferenceRoom(@PathVariable(name = "name") String roomName, HttpServletResponse response) {
		return roomService.findRoomByName(roomName, response);
	}

	@RequestMapping(method = RequestMethod.POST, path = "/filter")
	public ConferenceRoom searchConferenceRoomByFilter(@RequestBody RoomFilter filter, HttpServletResponse response) {
		LOGGER.info("Received filter {}", filter.toString());
		return roomService.searchBy(filter, response);
	}
}
