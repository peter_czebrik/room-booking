package cz.czebrik.roombooking.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import cz.czebrik.roombooking.model.ConferenceRoom;

@Repository
public interface ConferenceRoomRepository extends MongoRepository<ConferenceRoom, String>, ConferenceRoomRepositoryCustom {

	ConferenceRoom findByRoomName(String roomName);
}
