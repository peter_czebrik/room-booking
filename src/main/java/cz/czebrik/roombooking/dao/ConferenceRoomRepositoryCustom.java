package cz.czebrik.roombooking.dao;

import cz.czebrik.roombooking.model.ConferenceRoom;
import cz.czebrik.roombooking.model.RoomFilter;

public interface ConferenceRoomRepositoryCustom {

	ConferenceRoom searchBy(RoomFilter filter);
}
