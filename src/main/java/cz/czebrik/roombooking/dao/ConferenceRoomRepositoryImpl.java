package cz.czebrik.roombooking.dao;

import static org.springframework.data.mongodb.core.query.Criteria.where;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import cz.czebrik.roombooking.model.ConferenceRoom;
import cz.czebrik.roombooking.model.RoomFilter;

/**
 * Impl of mongo template usage for complex search by multiple params
 */
@Repository
public class ConferenceRoomRepositoryImpl implements ConferenceRoomRepositoryCustom {

	private MongoOperations mongoOperations;

	@Autowired
	public ConferenceRoomRepositoryImpl(MongoOperations mongoOperations) {
		this.mongoOperations = mongoOperations;
	}

	@Override
	public ConferenceRoom searchBy(RoomFilter filter) {
		List<Criteria> criteriaList = new ArrayList<>();
		if (!StringUtils.isEmpty(filter.getRoomName())) {
			criteriaList.add(where("roomName").regex(filter.getRoomName(), "i"));
		}
		if (!StringUtils.isEmpty(filter.getRoomNumber())) {
			criteriaList.add(where("roomNumber").regex(filter.getRoomNumber(), "i"));
		}
		criteriaList.add(where("capacity").gte(filter.getCapacity()));
		if (filter.isIncludesProjector()) {
			criteriaList.add(where("includesProjector").is(true));
		}
		if (filter.isIncludesFlipchart()) {
			criteriaList.add(where("includesFlipchart").is(true));
		}
		Criteria criteria = new Criteria().andOperator(criteriaList.toArray(new Criteria[criteriaList.size()]));
		Query query = new Query(criteria);
		/*
		 * Simplified to return only one result every time
		 */
		return mongoOperations.findOne(query, ConferenceRoom.class);
	}
}
