package cz.czebrik.roombooking.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RoomFilter {

	private String roomName;

	private String roomNumber;

	private int capacity;

	private boolean includesProjector;

	private boolean includesFlipchart;

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public boolean isIncludesProjector() {
		return includesProjector;
	}

	public void setIncludesProjector(boolean includesProjector) {
		this.includesProjector = includesProjector;
	}

	public boolean isIncludesFlipchart() {
		return includesFlipchart;
	}

	public void setIncludesFlipchart(boolean includesFlipchart) {
		this.includesFlipchart = includesFlipchart;
	}

	@Override
	public String toString() {
		return "RoomFilter{" +
				"roomName='" + roomName + '\'' +
				", roomNumber='" + roomNumber + '\'' +
				", capacity=" + capacity +
				", includesProjector=" + includesProjector +
				", includesFlipchart=" + includesFlipchart +
				'}';
	}
}
