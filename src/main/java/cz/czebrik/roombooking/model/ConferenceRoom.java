package cz.czebrik.roombooking.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Document(collection = "conferenceRoom")
public class ConferenceRoom {

	@Id
	@JsonSerialize(using = ToStringSerializer.class)
	private ObjectId id;

	private String roomName;

	private String roomNumber;

	private String mapUrl;

	private String phoneNumber;

	private int capacity;

	private boolean includesProjector;

	private boolean includesFlipchart;

	ConferenceRoom() {
		// Used in tests, package visibility
	}

	public ConferenceRoom(String roomName, String roomNumber, String mapUrl, String phoneNumber, int capacity, boolean includesProjector, boolean includesFlipchart) {
		this.roomName = roomName;
		this.roomNumber = roomNumber;
		this.mapUrl = mapUrl;
		this.phoneNumber = phoneNumber;
		this.capacity = capacity;
		this.includesProjector = includesProjector;
		this.includesFlipchart = includesFlipchart;
	}

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}

	public String getMapUrl() {
		return mapUrl;
	}

	public void setMapUrl(String mapUrl) {
		this.mapUrl = mapUrl;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public boolean isIncludesProjector() {
		return includesProjector;
	}

	public void setIncludesProjector(boolean includesProjector) {
		this.includesProjector = includesProjector;
	}

	public boolean isIncludesFlipchart() {
		return includesFlipchart;
	}

	public void setIncludesFlipchart(boolean includesFlipchart) {
		this.includesFlipchart = includesFlipchart;
	}
}
