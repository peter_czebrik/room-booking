package cz.czebrik.roombooking.service;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cz.czebrik.roombooking.dao.ConferenceRoomRepository;
import cz.czebrik.roombooking.model.ConferenceRoom;
import cz.czebrik.roombooking.model.RoomFilter;

@Service
public class RoomService {

	private ConferenceRoomRepository roomRepository;

	@Autowired
	public RoomService(ConferenceRoomRepository roomRepository) {
		this.roomRepository = roomRepository;
	}

	public ConferenceRoom findRoomByName(String name, HttpServletResponse response) {
		ConferenceRoom room = roomRepository.findByRoomName(name);
		checkNotFound(response, room);
		return room;
	}

	public ConferenceRoom searchBy(RoomFilter filter, HttpServletResponse response) {
		ConferenceRoom room = roomRepository.searchBy(filter);
		checkNotFound(response, room);
		return room;
	}

	private void checkNotFound(HttpServletResponse response, ConferenceRoom room) {
		if (room == null) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		}
	}
}
